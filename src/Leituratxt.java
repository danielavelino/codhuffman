import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

//FUNÇÃO PARA LER OS DADOS DENTRO DE UM TXT.
//DEVE SER PASSADO O CAMINHO DA PASTA QUE ESTÁ O ARQUIVO
public class Leituratxt {
    public static String leitor(String path) throws IOException {
 
        Scanner in = new Scanner(new FileReader(path));
        String line ="";   
        while (in.hasNextLine()) {
            line = in.nextLine();
           // System.out.println(line);
            
        } 
        return line;
     
    }

    // FUNÇÃO PARA ESCREVER OS DADOS DENTRO DE UM TXT.

    public static void escritor(String path, String msg) throws IOException {
        BufferedWriter buffWrite = new BufferedWriter(new FileWriter(path, true));
        String linha = "";
        linha = msg;
        //System.out.println(linha);
        buffWrite.append(linha + "\n");
        buffWrite.close();
    }

}
