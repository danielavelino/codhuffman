
public class Processamento {
    /*
     * Implementação do Algoritmo de Huffman CompactaÇão e Descompatação de um texto    
     */
    public static void main(String[] args) throws Exception {
        String test = (Leituratxt.leitor("./src/nomes.txt")); // O texto a ser compactado

        // Passo 1 - Percorre o texto contando os símbolos e montando um vetor de frequências
    
        int[] charFreqs = new int[256];
        for (char c : test.toCharArray())
            charFreqs[c]++;

        // Criar a Árvore dos códigos para a Compactação
        Arvore arvore = montarArvore(charFreqs);

        // Resultados das quantidade e o código da Compactação
        System.out.println("TABELA DE CÓDIGOS");
        System.out.println("SIMBOLO\tQUANTIDADE\tHUFFMAN CÓDIGO");
        printCodes(arvore, new StringBuffer());

        // Compactar o texto
        String compactar[] = compactar(arvore, test);
        // Mostrar o texto Compactado
        System.out.println("\nTEXTO COMPACTADO");
        System.out.println(compactar[0]+"- TAMANHO :"+compactar[1]+" bits"); 
        Leituratxt.escritor("./src/compactacao.txt", compactar[0]);
        // Decodificar o texto
        System.out.println("\n\nTEXTO DECODIFICADO");
        System.out.println(descompactar(arvore, compactar[0]) + "TAMANHO : " + (test.length()*8)+" bits");

    }

    /*
        ESTE MÉTODO MONTA, CONSTROE A ÁRVORE ATRAVEZ DAS LETRAS QUE FORAM ARMAZENADAS DENTRO DO 
        VETOR DE LETRAS CONFORME TABELA ASCII
     */
    public static Arvore montarArvore(int[] charFreqs) throws Exception {

        Lista<Arvore> lista = new Lista<Arvore>();

          // // Criar as Folhas da Árvore para cada letra
        for (int i = 0; i < charFreqs.length; i++) {
            if (charFreqs[i] > 0)
               
                //ADICIONA AS FOLHAS NA LISTA
                lista.adicionarLista(new Folha(charFreqs[i], (char) i));
        }
        //APENAS EXIBIR O TEXTO SEM ORDENAÇÃO
        System.out.println("SEM ORDENAÇÃO");

        for (int i = 0; i < lista.getTamanho(); i++) {
            System.out.println("QTDE : " + lista.get(i).frequency + " | LETRA " + lista.get(i).value);
        }

        // PASSAR LISTA COMO PARÂMETRO NO MÉTODO PARA ATUALIZAR A LISTA
        lista.ordenarLista(lista); // ORDENAR A LISTA

        System.out.println("\nCOM ORDENAÇÃO");
        for (int i = 0; i < lista.getTamanho(); i++) {
            System.out.println("QTDE : " + lista.get(i).frequency + " | LETRA " + lista.get(i).value);
        }
        // CRIAR A ARVORE
        while (lista.getTamanho() > 1) {
            //EXIBINDO NO CONSOLE A REMOÇÃO DOS NÓS E FOLHAS CONFORME A ÁRVORE VAI SE FORMANDO
            System.out.println("Tamanho antes de remover o nó: " + lista.getTamanho());
            //PEGA SEMPRE OS DOIS NÓS COM MENOR FREQUÊNCIA, POIS A LISTA ORDENA LOGO APÓS A REMOÇÃO.
            Arvore a = lista.remover(lista.getPrimeiro().value); // Pega o nó ou folha e salva numa variável e logo
                                                                 // depois remove este nó/folha da lista

            Arvore b = lista.remover(lista.getPrimeiro().value); // Pega o nó ou folha e salva numa variável e logo
                                                                 // depois remove este nó/folha da lista

            //CRIA OS NÓS DA ÁRVORE
            lista.adicionarLista(new Folha(a, b));

            // PASSAR LISTA COMO PARÂMETRO NO MÉTODO PARA ATUALIZAR A LISTA
            lista.ordenarLista(lista);
            // CADA VEZ QUE REMOVE A LISTA....ELA É ORDENADA E DEPOIS EXIBINA NO CONSOLE.
            System.out.println("\nCOM ORDENAÇÃO - NÓ");
            for (int i = 0; i < lista.getTamanho(); i++) {
                System.out.println("QTDE : " + lista.get(i).frequency + " | LETRA " + lista.get(i).value);
            }
            //EXIBIR A LISTA DE NÓ APÓS A REMOÇÃO PARA CONSTRUÇÃO DA ÁRVORE    
            System.out.println("Tamanho depois do nó: " + lista.getTamanho());

        }
        // RETORNA O PRIMEIRO NÓ DA LISTA
        return lista.remover(lista.getPrimeiro().value);
    }

    /*
     * FUNÇÃO PARA COMPACTAR A ÁRVORE
     */
    public static String[] compactar(Arvore tree, String compactar) {
        assert tree != null;
        String vet[] = new String[2];
        int soma=0;
        String encodeText = "";
        for (char c : compactar.toCharArray()) {
            encodeText += (getCodes(tree, new StringBuffer(), c));
            soma += 1;
        }
        vet[0] = encodeText;
        vet[1] = Integer.toString(soma);
        //return encodeText; // RETORNA O TEXTO COMPACTADO
        return vet; // RETORNA O TEXTO COMPACTADO
    }

    /*
     * FUNÇÃO PARA DESCOMPACTAR A ARVORE
     */
    public static String descompactar(Arvore arvore, String compactar) {
        assert arvore != null;

        String decodeText = "";
        Folha folhaNo = (Folha) arvore;

        for (char code : compactar.toCharArray()) {
            if (code == '0') { // Quando for igual a 0 é o Lado Esquerdo
                folhaNo = (Folha) folhaNo.esquerda;

                if ((folhaNo.esquerda == null) && (folhaNo.direita == null)) {
                    decodeText += ((Folha) folhaNo).value; // Retorna o valor do nó folha,pelo lado Esquerdo

                    folhaNo = (Folha) arvore; // Retorna para a Raíz da Árvore
                } else {
                    // Continua percorrendo a Árvore pelo lado Esquerdo
                }
            } else if (code == '1') { // Quando for igual a 1 é o Lado Direito

                folhaNo = (Folha) folhaNo.direita;
                if ((folhaNo.esquerda == null) && (folhaNo.direita == null))

                {
                    decodeText += ((Folha) folhaNo).value; // Retorna o valor do nó folha,
                    // pelo lado Direito
                    folhaNo = (Folha) arvore; // Retorna para a Raíz da Árvore
                } else {
                    // Continua percorrendo a Árvore pelo lado Direito
                }
            }
        } // End for
        return decodeText; // Retorna o texto Decodificado
    }

    /*
     * MÉTODO PARA PERCORRER A ÁRVORE E MOSTRAR A TABELA DE COMPACTAÇÃO.
     */
    public static void printCodes(Arvore arvore, StringBuffer prefix) {
        assert arvore != null;

        Folha folhaNo = (Folha) arvore;

        if ((folhaNo.esquerda == null) && (folhaNo.direita == null)) {

            // IMPRIMIR A LISTA NA TELA
            System.out.println(folhaNo.value + "\t" + folhaNo.frequency + "\t\t" + prefix);

        } else

        if ((folhaNo.esquerda != null) && (folhaNo.direita != null)) {

            // LADO ESQUERDO
            prefix.append('0');
            printCodes(folhaNo.esquerda, prefix);// FUNÇÃO RECURSIVA
            prefix.deleteCharAt(prefix.length() - 1);

            // LADO DIREITO
            prefix.append('1');
            printCodes(folhaNo.direita, prefix); // FUNÇÃO RECURSIVA
            prefix.deleteCharAt(prefix.length() - 1);
        }
    }

    /*
     * MÉTODO PARA RETORNAR O CÓDIGO COMPACTADO (LETRA). CODIFICAÇÃO DO TEXTO : 0
     * e/ou 1 - LETRA: prefix- Letra codificada
     */
    public static String getCodes(Arvore arvore, StringBuffer prefix, char w) {
        assert arvore != null;

        Folha folhaNo = (Folha) arvore;

        if ((folhaNo.esquerda == null) && (folhaNo.direita == null)) {
            // RETORNA O TEXTO DA LETRA COMPACTADO
            if (folhaNo.value == w) {
                return prefix.toString();
            }

        } else

        if ((folhaNo.esquerda != null) && (folhaNo.direita != null)) {
            // PERCORRE PELA ESQUERDA
            prefix.append('0');
            String esquerda = getCodes(folhaNo.esquerda, prefix, w);
            prefix.deleteCharAt(prefix.length() - 1);

            // PERCORRE PELA DIREITA
            prefix.append('1');
            String direita = getCodes(folhaNo.direita, prefix, w);
            prefix.deleteCharAt(prefix.length() - 1);

            if (esquerda == null)
                return direita;
            else
                return esquerda;
        }
        return null;
    }

}
