/*
 * Classe nó da árvore
 */
class No extends Arvore {
    public  Arvore esquerda, direita; // SUB - ÁRVORE

    //RECUPERAR O LADO ESQUERDO DO NÓ DA ARVORE
    public Arvore getEsquerda() {
        return this.esquerda;
    }

    //RECUPERAR O LADO DIREITO DO NÓ DA ARVORE
    public Arvore getDireita() {
        return this.direita;
    }

    // QUANDO O LADO FOR UM NÓ, ESTE MÉTODO SOMA AS FREQUENCIAS E SALVA O LADO DIREITO E ESQUERDO ( FREQ , VALOR)
    public No(Arvore l, Arvore r) {
        super(l.frequency + r.frequency);
        this.esquerda = l;
        this.direita = r;
    }
    //QUANDO FOR UMA FOLHA, NÃO TERÁ LADO DIREITO E NEM ESQUERDO
    public No(int f) {// folha
        super(f);
        esquerda = null;
        direita = null;
    }
    //MUDA O LADO ESQUERDO DO NÓ
    public void setEsquerda(Arvore e) {
        this.esquerda = e;
    }

    //MUDA O LADO DIREITO DO NÓ
    public void setDireita(Arvore d) {
         this.direita = d;
    }
 
}