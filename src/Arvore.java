/*
 * Classe abstrata da �rvore 
 * De forma objetiva uma classe abstrata serve como modelo para uma classe concreta, neste caso para as classes NODE e LEAF
 */
abstract class Arvore implements Comparable<Arvore> {
    //public final int frequency; // Frequ�ncia da �rvore
    int frequency; // Frequ�ncia da �rvore
    //
    public Arvore(int freq) { 
    	frequency = freq; 
    }
    public int getFrequency() {
        return this.frequency;
    }    
    public void setFrequency(int freq) {
        this.frequency = freq ;
    }    
    // Compara as frequ�ncias - Implementa��o da Interface Comparable para a ordena��o na fila
    public int compareTo(Arvore arvore) {
        return frequency - arvore.frequency;
    }
}