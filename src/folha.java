/*
 * Classe referente ao nó da folha
 */
class Folha extends No {  
    char value; // Letra a um nó da folha
    Folha proximo;
   //O Construtor vazio será chamando quando for um nó, pois o nó não tem valor. Tem direita e esquerda
    public Folha() {
        super(0);
        this.value = ' ';
    }
    //Quando for folha será armazenado a freqência e o valor
    public Folha(int freq, char val) {
        super(freq);
        this.value = val;
    }

    //Quando for nó, será acrescentado o lado direito e esquerdo da arvore
    public Folha(Arvore l, Arvore r) {//No
        super(l, r);   
        this.value = ' ';     
    }   

    //Este método pega o próximo nó da lista de arvore/nó/folha
    public Folha getProximo() {
        return proximo;
    }
    //método utilizado para mudar o próximo 
    public void setProximo(Folha proximo) {
        this.proximo = proximo;
    }

  //Método que retorna o valor atual  
    public char getValue() {
        return this.value;
    }
    //Método para mudar o valor
    public void setValue(char val) {
        this.value = val;
    }

}